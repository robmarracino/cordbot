package cordbot; // package in order to access all classes
//test
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;

public class Main extends ListenerAdapter { // extending listeneradapter lets us use JDAs event listeners
    private static JDA jda; // holds JDA in order to access information from any guild despite event
    private static String prefix = "c!"; // default global prefix


    public static void main(String[] args) throws LoginException { // login exception in case token isn't valid
        JDABuilder builder = new JDABuilder(); // build a new JDABuilder object
        builder.setToken("INSERT TOKEN HERE"); // set bot token for build
        // for the sake of organization, every command will be it's own class incorporating it's own listener
        builder.addEventListeners(new Main(), new Ping()); //
        jda = builder.build(); // builds JDA instance and assigns it to JDA
    }

    // event listener that reads every message the bot receives
    // this will be our default on message received event that logs to console every message the bot receives
    // this is also a somewhat example on how to access specific data from an event
    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        super.onMessageReceived(event);
        String guild = event.getGuild().getName(); // stores the name of the Guild(server) into variable guild
        String user = event.getAuthor().getAsTag(); // stores the name of the user who sent the message as a tag meaning it will show his full user name including the #
        String message = event.getMessage().getContentDisplay(); // stores the message received as appeared to a regular user, for raw content, use getContentRaw
        System.out.println(guild + ": " + user + ": " + message); // logs to console "guild: user: message"
    }

    // method to grab private jda from any other class
    public static JDA getJda() {
        return jda;
    }

    // method to grab private prefix
    public static String getPrefix() {
        return prefix;
    }
}
