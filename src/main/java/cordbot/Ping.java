package cordbot;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

// we use separate classes in order to stay organized, work on, debug, and update each individual command
// this is a very basic ping command, it doesnt show latency yet, but we will eventually work on this command to make it better
public class Ping extends ListenerAdapter {
    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        super.onMessageReceived(event);
        String prefix = Main.getPrefix(); // this is why we need package cordbot
        String command = prefix + "ping";
        if (event.getMessage().getContentDisplay().equals(command)) {
            // gets event, gets channel event happened in, send a message that reads "pong" and queues it
            // queueing is required whenever the bot sends or receives specific specific information, for example sending messages or requesting message history
            // you can also use .complete() but that is highly inadvisable because .complete() stops the thread, meaning the bot will stop everything else it's doing in order to fulfill this one request
            // queueing gives a bot the chance to keep up with many incoming requests at once and reply when possible
            event.getChannel().sendMessage("pong").queue();
        }
    }
}
